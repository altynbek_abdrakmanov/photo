class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :title
      t.references :users, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
