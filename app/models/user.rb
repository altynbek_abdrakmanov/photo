class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :name, presence: true, length: {maximum: 50}
  has_many :pictures
  has_many :notes

  after_create { |user| user.send_reset_password_instructions }
  def password_required?
    new_record? ? false : super
  end
end
