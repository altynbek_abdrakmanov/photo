class PicturesController < ApplicationController
  def index
    @pictures = Picture.paginate(page: params[:page], per_page: 2)
  end

  def new
    @picture = Picture.new
  end

  def create
    @picture = current_user.pictures.build(picture_params)

    if @picture.save
      redirect_to @picture
    else
      render 'new'
    end

  end

  def edit
    @picture = Picture.find(params[:id])
  end

  def update
    @picture = Picture.find(params[:id])

    if @picture.update(picture_params)
      redirect_to @picture
    else
      render 'edit'
    end
  end

  def show
    @picture = Picture.find(params[:id])
    @note = Note.new
    @notes = Note.all
    @average = average_rate(@picture)
  end

  def create_comment
    @note = current_user.notes.build(comment_params)
    if @note.save
      redirect_to root_path
    end
  end

  def user
    @user = User.find(params[:id])
  end

  def average_rate(picture)
    counter = 0
    rate = 0
    picture.notes.each do |note|
      counter += 1
      rate += note.rate.to_f
    end
    if rate == 0
      average = 0
    else
      average = rate/counter
    end
    return average
  end

  private

  def picture_params
    params.require(:picture).permit(:title, :image, :user_id)
  end

  def comment_params
    params.require(:note).permit(:description, :rate, :user_id, :picture_id)
  end

end
