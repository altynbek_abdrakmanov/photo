ActiveAdmin.register Note do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :description, :rate, :user_id, :picture_id

  form do |f|
    f.inputs do
      f.input :description
      f.input :rate
    end
    f.actions
  end


end
