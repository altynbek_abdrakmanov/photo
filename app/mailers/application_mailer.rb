class ApplicationMailer < ActionMailer::Base
  default from: "esdp4.2015@gmail.com"
  layout 'mailer'
end
